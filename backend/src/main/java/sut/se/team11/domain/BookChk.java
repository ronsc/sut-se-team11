package sut.se.team11.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;


@Entity
public class BookChk {
	@Id
	@GeneratedValue
	private Long id;
	
	@Column
	private Boolean ChkOutStat;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}


	public Boolean getChkOutStat() {
		return ChkOutStat;
	}

	public void setChkOutStat(Boolean chkOutStat) {
		ChkOutStat = chkOutStat;
	}
	
}
