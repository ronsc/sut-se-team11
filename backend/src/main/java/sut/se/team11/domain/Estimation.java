package sut.se.team11.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

public class Estimation {
	
	@Entity
	public class Ebook {
		@Id
		@GeneratedValue
		private Long id;
		
		@Column
		private Long point1;
		
		@Column
		private Long point2;
		
		@Column
		private Long point3;
		
		@Column
		private Long point4;
		
		@Column
		private Long point5;
		
		@Column
		private String suggestion;

		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}

		public Long getPoint1() {
			return point1;
		}

		public void setPoint1(Long point1) {
			this.point1 = point1;
		}

		public Long getPoint2() {
			return point2;
		}

		public void setPoint2(Long point2) {
			this.point2 = point2;
		}

		public Long getPoint3() {
			return point3;
		}

		public void setPoint3(Long point3) {
			this.point3 = point3;
		}

		public Long getPoint4() {
			return point4;
		}

		public void setPoint4(Long point4) {
			this.point4 = point4;
		}

		public Long getPoint5() {
			return point5;
		}

		public void setPoint5(Long point5) {
			this.point5 = point5;
		}

		public String getSuggestio() {
			return suggestion;
		}

		public void setSuggestio(String suggestion) {
			this.suggestion = suggestion;
		}
		
}
}
