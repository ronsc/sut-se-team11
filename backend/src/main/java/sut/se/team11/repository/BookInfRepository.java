package sut.se.team11.repository;

import org.springframework.data.repository.CrudRepository;

import sut.se.team11.domain.BookInf;


public interface BookInfRepository extends CrudRepository<BookInf, Long>{

}
