package sut.se.team11.repository;

import org.springframework.data.repository.CrudRepository;

import sut.se.team11.domain.Ebook;

public interface EbookRepository extends CrudRepository<Ebook,Long>{

}
