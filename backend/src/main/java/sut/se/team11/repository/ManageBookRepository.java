package sut.se.team11.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import sut.se.team11.domain.ManageBook;

@Repository
public interface ManageBookRepository extends CrudRepository<ManageBook, Long> {

	public List<ManageBook> findByid(Long id);
}
