package sut.se.team11.repository;

import org.springframework.data.repository.CrudRepository;

import sut.se.team11.domain.News;

public interface NewsRepository extends CrudRepository<News, Long> {

}
