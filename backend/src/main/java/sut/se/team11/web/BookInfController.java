package sut.se.team11.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import sut.se.team11.domain.BookInf;
import sut.se.team11.repository.BookInfRepository;

@RestController
public class BookInfController {

	@Autowired
	private BookInfRepository BookInfRep;
	
	@RequestMapping("/find/test")
	@ResponseBody
	public List<BookInf> showall() {
		return (List<BookInf>) BookInfRep.findAll();
	}
}
