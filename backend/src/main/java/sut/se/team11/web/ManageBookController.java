package sut.se.team11.web;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import sut.se.team11.domain.ManageBook;
import sut.se.team11.repository.ManageBookRepository;

@RestController
public class ManageBookController {

	@Autowired
	private ManageBookRepository repobook;
	
	@RequestMapping("/ManageBook/add")
	@ResponseBody
	public void add(@Valid @RequestBody ManageBook managebook){	
		repobook.save(managebook);
	}
	
	@RequestMapping("/ManageBook/show")
	@ResponseBody
	public List<ManageBook> show(){
		return (List<ManageBook>)repobook.findAll();
	}
	
	@RequestMapping("/ManageBook/delete/{id}")
	@ResponseBody
	public void delete(@PathVariable Long id){
		repobook.delete(id);
	}
	
	@RequestMapping("/ManageBook/find/{id}")
	@ResponseBody
	public List<ManageBook> edit(@PathVariable Long id){
		return (List<ManageBook>)repobook.findByid(id);
	}
	
	@RequestMapping("/ManageBook/update")
	@ResponseBody
	public void update(@Valid @RequestBody ManageBook managebook){
		repobook.save(managebook);
	}
}
