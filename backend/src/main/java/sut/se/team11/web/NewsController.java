package sut.se.team11.web;
	
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import sut.se.team11.domain.News;
import sut.se.team11.repository.NewsRepository;

@RestController
public class NewsController {
	@Autowired
	private NewsRepository newsRepo;
	
	@RequestMapping("/news/list")
	@ResponseBody
	public List<News> list() {
		return (List<News>) newsRepo.findAll();
	}
	
	@RequestMapping("/news/views/{id}")
	@ResponseBody
	public News countViews(@PathVariable("id") Long id) {
		News news = newsRepo.findOne(id);
		news.setViewsTotal(news.getViewsTotal() + 1);
		newsRepo.save(news);
		return (News) newsRepo.findOne(id);
	}
	
	@RequestMapping("/news/save")
	@ResponseBody
	public void save(@RequestBody News news) {
		newsRepo.save(news);
	}
	
	@RequestMapping("/news/delete/{id}")
	@ResponseBody
	public void delete(@PathVariable("id") Long id) {
		newsRepo.delete(id);
	}
}
