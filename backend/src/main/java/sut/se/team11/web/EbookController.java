package sut.se.team11.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import sut.se.team11.domain.Ebook;
import sut.se.team11.repository.EbookRepository;

@RestController
public class EbookController {
	
	@Autowired
	private EbookRepository ebookRepo;
	
	@RequestMapping("/ebook/list")
	@ResponseBody
	public List<Ebook> list() {
		return (List<Ebook>) ebookRepo.findAll();
	}
	
	@RequestMapping("/ebook/views/{id}")
	@ResponseBody
	public Ebook showbyid(@PathVariable("id") Long id) {
		return (Ebook) ebookRepo.findOne(id);
	}
	
	
}
