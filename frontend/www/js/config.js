app.config(function($sceDelegateProvider, $routeProvider, $locationProvider) {
	$sceDelegateProvider.resourceUrlWhitelist(['self']);

	$routeProvider
	.when('/login', {
		templateUrl : '_login.html'
	})
	.when('/searching', {
		templateUrl : 'searching.html'
	})
	.when('/result', {
		templateUrl : 'result.html'
	})
	.when('/BookManage', {
		templateUrl : 'admin/BookManage.html'
	})
	.when('/NewsManage', {
		templateUrl : 'admin/NewsManage.html'
	})
	.when('/user', {
		templateUrl : 'user.html'
	})
	.when('/index', {
		templateUrl : '_news.html'
	})
	.when('/download_exam', {
		templateUrl : 'download_exam.html'	
	})
	.otherwise({
       redirectTo: '/index'
    })

})