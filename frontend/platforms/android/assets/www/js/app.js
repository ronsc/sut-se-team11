'use strict'

var app = angular.module('app', ['onsen', 'ngRoute']);

app.factory('Shared', function($http) {
	var id = 0
	var url = 'http://localhost:8080'
	var detail
	
	return {
		setId : function(val) {
			id = val
		},
		getId : function() {
			return id
		},
		getUrl : function() {
			return url
		},
		setDetail : function(val) {
			detail = val
		},
		getDetail : function() {
			return detail
		}
	}
})

app.controller('menuCtrl', function($scope, $route, $location, Auth) {
	$scope.gotoPage = function(path) {
		$location.path('/' + path)
		app.menu.closeMenu()
	}
	
	$scope.user = Auth.isLoggedIn()
})

app.controller('mainCtrl', function($scope, Auth, $location, $route) {
	$scope.$watch(Auth.isLoggedIn, function(value, oldValue) {

		if(!value && oldValue) {
			console.log('Disconnect')
			$location.path('/login')
		} 

		if(value) {
			console.log('Connect')
		}

	}, true)

	this.$route = $route;
    this.$location = $location;
})