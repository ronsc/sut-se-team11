app.factory('Auth', function() {
	var user

	return {
		setUser : function(aUser) {
			user = aUser
		},
		isLoggedIn : function() {
			return (user) ? user : false
		}
	}
})

app.run(function($rootScope, $location, Auth, $route) {
	
	$rootScope.$on('$routeChangeStart', function(event) {

		var user = Auth.isLoggedIn()

		if(!user) {
			console.log('DENY')
			event.preventDefault()
			$location.path($location.path())
		} else {
			console.log('ALLOW')
			var path = $location.path()
			
			if(user.level == 'ADMIN')
				app.menu.setMenuPage('menu_admin.html')
			else
				app.menu.setMenuPage('menu_user.html')

			if(path == '/logout') {
				Auth.setUser(false)
				app.menu.setMenuPage('menu_user.html')
				app.menu.setMainPage('_sub.html')
				$location.path('/index')
			} else {
				$location.path(path)
			}
			
		}

	})

})

app.controller('loginCtrl', function($scope, Auth, $route, $location) {

	$scope.login = function() {
		var user = {userName : 'user', name : 'user user', level : 'USER'}
		var admin = {userName : 'admin', name : 'admin admin', level : 'ADMIN'}

		dialog.hide()
		if($scope.username == 'admin') {
			Auth.setUser(admin)
			$location.path('/user')
		} else if($scope.username == 'user') {
			Auth.setUser(user)
			$location.path('/user')
		} else {
			ons.notification.alert({message: 'Username/Password not Wrong!'})
		}
		
	}

})