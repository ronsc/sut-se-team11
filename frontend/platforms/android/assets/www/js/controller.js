/* ------------- Users ------------- */
app.controller('userCtrl', function($scope, Auth) {
	$scope.user = Auth.isLoggedIn()
})
/* --------------------------------------- */

/* ------------- News ------------- */
app.controller('NewsCtrl', function($scope, $http, Shared, Auth) {
	var url = Shared.getUrl()
	$scope.user = Auth.isLoggedIn()
	var id = Shared.getId()

	$scope.detail = function(id) {
		var path = '/news/views/' + id

		$http.get(url + path).success(function(data) {
			Shared.setDetail(data)
			Shared.setId(id)
			app.navi.pushPage('news_detail.html')
		})

	}

	$scope.edit = function(id) {
		var path = '/news/views/' + id

		$http.get(url + path).success(function(data) {
			Shared.setDetail(data)
			Shared.setId(id)
			myNav.pushPage('admin/news_edit.html', {animation: 'slide'})
		})

	}

	$scope.show = function(dlg) {
    	ons.createDialog(dlg).then(function(dialog) {
	    	dialog.show();
	    });
	}
	
	$scope.list = function() {
		var path = '/news/list'
		$http.get(url + path).success(function(data) {
			$scope.datas = data
		})
	}
	$scope.list()
})

app.controller('NewsDetailCtrl', function($scope, $http, Shared) {
	Shared.setId(0)

	$scope.datas = Shared.getDetail()
})

app.controller('NewsManageCtrl', function($scope, $http, Shared) {
	var id = Shared.getId()
	var url = Shared.getUrl()

	if(id != 0) {
		var detail = Shared.getDetail()
		
		console.log(detail)

		$scope.id 			= detail.id
		$scope.title 		= detail.title
		$scope.imageUrl 	= detail.imageUrl
		$scope.content 		= detail.content
		$scope.viewsTotal 	= detail.viewsTotal

		console.log(myNav)
	}

	$scope.save = function() {
		var path = '/news/save'
		
		if(id == 0) {
			var data = {
				title 		: $scope.title,
				imageUrl 	: $scope.imageUrl,
				content 	: $scope.content,
				viewsTotal 	: 0
			}
		} else {
			var data = {
				id 			: $scope.id,
				title 		: $scope.title,
				imageUrl 	: $scope.imageUrl,
				content 	: $scope.content,
				viewsTotal 	: $scope.viewsTotal
			}
		}
		console.log(data)

		$http.post(url + path, data).success(function(data) {
			Shared.setId(0)
			myNav.popPage()
		})
	}

	
	$scope.delete = function() {
		var path = '/news/delete/' + id

		$http.get(url + path).success(function(data) {
			ons.notification.alert({message: 'ลบข่าวสำเร็จ'})
			myNav.popPage()
		})
	}
	
})
/* --------------------------------------- */

/* ------------- list-searching-script ------------- */
app.controller('DialogController', function($scope) {
  
  $scope.show = function(dlg) {
    ons.createDialog(dlg).then(function(dialog) {
      dialog.show();
    });
  }
  
});
/* --------------------------------------- */

/* ------------- Book Manage ------------- */
app.controller('Bookmanage',function($scope,$http){
	$scope.add = function(){
		var data = {
			name   :  $scope.name,
			bookid :  $scope.id,
			author :  $scope.author,
			number :  $scope.number
		};
		
		$http.post('http://localhost:8080/ManageBook/add',data).success(function(data){
			alert("add success");
			ons.slidingMenu.setMainPage('BookManage.html', {closeMenu: true});
		}).error(function(data,status,headers,config){
			alert("error");
		});
	}
});
  
app.controller('Bookshow',function($scope,$http){
	$scope.show = function(){
		$http.get('http://localhost:8080/ManageBook/show').success(function(data){
					$scope.datas = data;
		}).error(function(data,status,headers,config){
				alert("error");
			});
	}
	$scope.show();

	$scope.Delete = function(id){
		$http.get('http://localhost:8080/ManageBook/delete/'+id).success(function(data){
			alert("Delete success");
			ons.slidingMenu.setMainPage('BookManage.html', {closeMenu: true});
		}).error(function(data,status,headers,config){
			alert("error");
		});
	}

	$scope.find = function(id){
		$http.get('http://localhost:8080/ManageBook/find/'+id).success(function(data){
			$scope.datas = data;
			$scope.ShowData.show()
		}).error(function(data,status,headers,config){
			alert("error");
		});
	}

	$scope.edit = function(id,name,bookid,number,author){
		var data1 = {
			id	   :  id,
			name   :  name,
			bookid :  bookid,
			author :  author,
			number :  number
		};
				
		$http.post('http://localhost:8080/ManageBook/update',data1).success(function(data){
			alert("Update success");
			ons.slidingMenu.setMainPage('BookManage.html', {closeMenu: true});
		}).error(function(data,status,headers,config){
			alert("error");
		});
	}
});
/* --------------------------------------- */
<<<<<<< HEAD
/*-----------------Exam--------------------*/
app.controller('searching_exam',function($scope,$http,Shared){
	$scope.searching = function(major){
		$http.get('http://localhost:8080/exam/'+ major).success(function(data){
					$scope.datas = data;
		}).error(function(data,status,headers,config){
				alert("error");
		});
	}
});
=======

/* -------------------BookSearching-------------------- */
	app.controller('BookSearchingCtrl', function($scope, $http){
		$scope.showall = function() {
			$http.get("http://localhost:8080/find/test").success(function(data){
				$scope.datas = data;
			}).error(function (data, status, headers, config){
				console.log("ERR:" + status);
				console.log("ERR:" + herders);
			});
		}
		
		$scope.showall();
		
	})
	
	app.controller('SelectSearch', function($scope, $http,Shared){
		$scope.Selected = function(x){
			var select = x;
			if(select == 1){ select = 'ชื่อหนังสือ' }
			alert(select);
			dialog.hide();
		}
	})

>>>>>>> fixed my UI & Controller
/* --------------------------------------- */